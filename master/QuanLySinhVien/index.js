/**
Input : diem chuan,diem 3 mon hoc,doi tuong uu tien,khu vuc uu tien


Các bước xử lí
-Dom tới các thẻ để nhận value
-Dùng hàm if,else if,else
Tổng điểm = điểm 3 môn + điểm khu vực ưu tiên + điểm đối tượng ưu tiên
+ If (3 môn có môn nhỏ hơn hoặc bằng 0) =>rớt
else if(tổng điểm < điểm chuẩn) =>rớt
else => đỗ
*/
function xepLoai() {
  var diemChuanValue = document.getElementById("txt-diem-chuan").value * 1;
  console.log("diemChuan", diemChuanValue);
  var diemMon1Value = document.getElementById("txt-mon-1").value * 1;
  console.log("diemMon1Value", diemMon1Value);
  var diemMon2Value = document.getElementById("txt-mon-2").value * 1;
  console.log("diemMon2Value", diemMon2Value);
  var diemMon3Value = document.getElementById("txt-mon-3").value * 1;
  console.log("diemMon3Value", diemMon3Value);
  var khuVucUuTienValue = document.getElementById("txt-khu-vuc").value * 1;
  var doiTuongUuTienValue = document.getElementById("txt-doi-tuong").value*1;
  var tongDiem =
    diemMon1Value +
    diemMon2Value +
    diemMon3Value +
    khuVucUuTienValue +
    doiTuongUuTienValue;
  if (diemMon1Value <= 0 || diemMon2Value <= 0 || diemMon3Value <= 0) {
    document.getElementById(
      "txt-xepLoai"
    ).innerHTML = `Bạn đã rớt.Do có điểm nhỏ hơn bằng 0`;
  } else if (tongDiem < diemChuanValue) {
    document.getElementById(
      "txt-xepLoai"
    ).innerHTML = `Bạn đã rớt .Tổng điểm :${tongDiem}`;
  } else {
    document.getElementById(
      "txt-xepLoai"
    ).innerHTML = `Bạn đã đỗ .Tổng điểm :${tongDiem}`;
  }
}
