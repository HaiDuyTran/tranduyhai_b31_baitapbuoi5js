/**
Input : Họ tên , số kw


Các bước xử lý:
-Dom để nhận giá trị từ input
-If (số kw < 0) => hiển thị lỗi 
else if (số kw <=50) =>Tiền điện = số kw*500
else if (số kw <=100) =>Tiền điện = số kw*500+(số kw-50)*650
else if (số kw <=150) =>Tiền điện = số kw*500+(số kw-50)*650+(số kw-100)*850
else if (số kw <=200) =>Tiền điện = số kw*500+(số kw-50)*650+(số kw-100)*850+(số kw-150)*1100
else  =>Tiền điện = số kw*500+(số kw-50)*650+(số kw-100)*850+(số kw-150)*1100(số kw-200)*1300

 */



function tinhTienDien() {
  var hoTen = document.getElementById("txt-ho-ten").value;
  var sokwValue = document.getElementById("txt-so-kw").value * 1;
  var tienDien = 0;
  if (sokwValue <= 0) {
    alert("Số kw không hợp lệ , vui lòng nhập lại");
  } else if (sokwValue <= 50) {
    tienDien = sokwValue * 500;
  } else if (sokwValue <= 100) {
    tienDien = sokwValue * 500 + (sokwValue - 50) * 650;
  } else if (sokwValue <= 150) {
    tienDien =
      sokwValue * 500 + (sokwValue - 50) * 650 + (sokwValue - 100) * 850;
  } else if (sokwValue <= 200) {
    tienDien =
      sokwValue * 500 +
      (sokwValue - 50) * 650 +
      (sokwValue - 100) * 850 +
      (sokwValue - 150) * 1100;
  } else {
    tienDien =
      sokwValue * 500 +
      (sokwValue - 50) * 650 +
      (sokwValue - 100) * 850 +
      (sokwValue - 150) * 1100 +
      (sokwValue - 200) * 1300;
  }

console.log(tienDien.toLocaleString());
  document.getElementById(
    "txt-ket-qua"
  ).innerHTML = `Họ tên : ${hoTen} ; Tiền điện :${tienDien} `;
}
